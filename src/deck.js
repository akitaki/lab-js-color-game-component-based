import Component from './component.js';
import Card from './card.js';

import './deck.css';

export default class Deck extends Component {
  static getRootClass() {
    return '.deck';
  }

  constructor(root) {
    super(root);

    /** @type {NodeListOf.<HTMLDivElement>} */
    this.cardRows = root.querySelectorAll('.card-row');

    const cardElements = [...root.querySelectorAll(Card.getRootClass())];
    this.cards = cardElements.map((el, index) => {
      const card = new Card(el, index);
      card.on('click', this.handleCardClick.bind(this));
      return card;
    });
  }

  /**
   * @param {string[]} candidateColors
   * @param {boolean[]} candidateVisibilities
   */
  update(candidateColors, candidateVisibilities) {
    candidateColors.forEach((color, index) => {
      let card = this.cards[index];
      if (candidateVisibilities[index]) {
        card.fadeIn(color);
      } else {
        card.fadeOut();
      }
    });
  }

  handleCardClick(_firer, index) {
    this.fire('click', index);
  }

  hideSecondRow() {
    this.cardRows[1].classList.add('hidden');
  }

  showSecondRow() {
    this.cardRows[1].style.opacity = 1;
    this.cardRows[1].classList.remove('hidden');
  }
}
