export default class Component {
  /*
   * Override this method
   */
  static getRootClass() {
    return '.component';
  }

  constructor(root) {
    /** @param {HTMLElement} */
    this.root = root;
    this.handlers = {};
  }

  on(event, handler) {
    this.handlers[event] = handler;
  }

  fire(event, ...args) {
    this.handlers[event](this, ...args);
  }
}
