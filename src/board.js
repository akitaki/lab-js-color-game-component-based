import Component from './component.js';

import './board.css';

export default class Board extends Component {
  static getRootClass() {
    return '.board';
  }

  constructor(root) {
    super(root);

    this.colorDisplay = root.querySelector('.color-picked');
    this.messageDisplay = root.querySelector('.message');
    /** @type {HTMLSpanElement} */
    this.countdownDisplay = root.querySelector('.countdown');
  }

  showColor(color) {
    this.colorDisplay.textContent = color;
  }

  /**
   * Show the correct message and clear the countdown display, along with its
   * left padding.
   */
  showCorrectMessage() {
    this.messageDisplay.textContent = 'Correct!';
    this.countdownDisplay.textContent = '';
    this.countdownDisplay.style.paddingLeft = '0rem';
  }

  /**
   * Show the timeout message and clear the countdown display, along with its
   * left padding.
   */
  showTimeoutMessage() {
    this.messageDisplay.textContent = 'Timeout';
    this.countdownDisplay.textContent = '';
    this.countdownDisplay.style.paddingLeft = '0rem';
  }

  /**
   * Set countdown display to show `seconds`.  Left padding for the display is
   * also added.
   * @param {number} seconds
   */
  showCountdown(seconds) {
    this.countdownDisplay.textContent = seconds.toString();
    this.countdownDisplay.style.paddingLeft = '1rem';
  }

  showWrongMessage() {
    this.messageDisplay.textContent = 'Try Again';
  }

  showOriginal() {
    this.messageDisplay.textContent = "What's the color?";
  }
}
